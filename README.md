# Agnoia

Agnoia is a simple side-scrolling action platform that take place in a future distopian society were only the best detectives can tell right from wrong.

This is a project made for the exam 'Human Computer Interaction' during my master thesis in Engineering in Computer Science at Sapienza Università di Roma back in 2021. Is a game developed with Unreal Engine 4, if you want to play the game follow the instruction below.


# Download and play

The build works only on Windows, to play the game you must:

- Clone or Download & Unzip the repository    
- Run the file 'Agnoia' within

*If the file 'Agnoia' doesn't work you can try to run the other one inside 'WindowsBuild'

## My Work
The game was made by a team of 4 students, my work was to make most of the game code, I created the character controls, the weapons, the logic behind the menus, the behaviour of some enemies, the design of the tutorial level and the final boss fight, I also made most of the sound effects and HUD.
